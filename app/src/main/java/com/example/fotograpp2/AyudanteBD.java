package com.example.fotograpp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AyudanteBD extends SQLiteOpenHelper {
    private static final String NOMBRE_TABLA_PLANTAS = "plantas";
    private static final String NOMBRE_BASE_DE_DATOS = "plantas";
    private static final int VERSION_BASE_DE_DATOS = 1;

    public AyudanteBD(Context context) {
        super(context, NOMBRE_BASE_DE_DATOS, null, VERSION_BASE_DE_DATOS);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(String.format("CREATE TABLE IF NOT EXISTS %s(id integer primary key autoincrement, path text, fecha text, tipo text, nombre text)", NOMBRE_TABLA_PLANTAS));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //Metodo para cargar fotos a la BD
    public boolean insertarFotos(String path, String fecha, String tipo, String nombre){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues valor = new ContentValues();
        valor.put(path, path);
        valor.put(fecha, fecha);
        valor.put(tipo, tipo);
        valor.put(nombre, nombre);
        long resultado = db.insert(NOMBRE_BASE_DE_DATOS, null, valor);
        if (resultado != -1){
            return true;
        } else {
            return false;
        }
    }

    //Metodo para Leer el contenido de la BD
    public Cursor getDatos(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor resultado = db.rawQuery("select * from " + NOMBRE_TABLA_PLANTAS, null);
        return resultado;
    }
}
