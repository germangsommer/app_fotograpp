package com.example.fotograpp2;

import android.content.Intent;
import android.database.Cursor;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Switch;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public List<Foto> listaDeFotos;


    AyudanteBD bd = new AyudanteBD(this);
    Camera camara;
    FrameLayout frameLayout;
    Switch switchArbol;
    VerCamara verCamara;
    String path = Environment.getExternalStorageDirectory() + File.separator + "Arboles";
    Button botonGaleria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        switchArbol = (Switch) findViewById(R.id.switchArbol);
        botonGaleria = (Button) findViewById(R.id.botonGaleria);

        //Abrir la camara

        camara = Camera.open();
        verCamara = new VerCamara(this, camara);
        frameLayout.addView(verCamara);

        switchArbol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiarTipo();
            }
        });
        verGaleria();

    }

    public List<Foto> getListaDeFotos() {
        return listaDeFotos;
    }

    Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File archivoFoto = getOutputMediaFile();
            if (archivoFoto == null){
                return;
            } else {
                try{
                    FileOutputStream fos = new FileOutputStream(archivoFoto);
                    fos.write(data);
                    fos.close();
                    camara.startPreview();
                    galleryAddPic();
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    };

    private File getOutputMediaFile() {
        String estado = Environment.getExternalStorageState();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "Imagen_" + timeStamp + ".jpg";
        if (!estado.equals(Environment.MEDIA_MOUNTED)){
            return null;
        } else {
            File carpeta_gui = new File(path);
            if (!carpeta_gui.exists()){
                carpeta_gui.mkdirs();
            }
            File ArchivoSalida = new File(carpeta_gui,fileName);
            //Crear objeto de tipo Foto
            Foto foto = new Foto(path, timeStamp.toString(), switchArbol.getText().toString());
            //Carga y control de datos a la BD
            boolean carga = bd.insertarFotos(foto.getPath(), foto.getFecha(), foto.getTipo(), foto.getTipo() + "_" + foto.getFecha());
            if (carga != true){
                Toast.makeText(MainActivity.this, "Foto Cargada", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(MainActivity.this, "Error al Cargar", Toast.LENGTH_LONG).show();
            }
            return ArchivoSalida;
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(path);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    public void capturarImagen(View v){
        if (camara != null) {
            camara.takePicture(null, null, mPictureCallback);
        }
    }

    public void cambiarTipo(){
        if (switchArbol.isChecked()){
            path = Environment.getExternalStorageDirectory() + File.separator + "Arbustos";
            switchArbol.setText("Arbustos");
        } else {
            path = Environment.getExternalStorageDirectory() + File.separator + "Arboles";
            switchArbol.setText("Arboles");
        }
    }

    public void verGaleria(){
        botonGaleria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor resultado = bd.getDatos();
                while (resultado.moveToNext()){
                    Foto foto = new Foto(resultado.getString(0),resultado.getString(1), resultado.getString(2));
                    listaDeFotos.add(foto);
                }
                Intent intent = new Intent(MainActivity.this, verGaleriaActivity.class);
                startActivity(intent);
            }
        });
    }
}
