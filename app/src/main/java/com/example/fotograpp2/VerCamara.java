package com.example.fotograpp2;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.List;

public class VerCamara extends SurfaceView implements SurfaceHolder.Callback {

    Camera camara;
    SurfaceHolder holder;

    public VerCamara(Context context, Camera camara) {
        super(context);
        this.camara = camara;
        holder = getHolder();
        holder.addCallback(this);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        camara.stopPreview();
        camara.release();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Camera.Parameters parametros = camara.getParameters();

        //Cambiar la orientacion de la camara

        List<Camera.Size> tamanos = parametros.getSupportedPictureSizes();
        Camera.Size mTamano = null;

        for(Camera.Size tamano : tamanos){
            mTamano = tamano;
        }

        if(this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE){
            parametros.set("orientation", "portrait");
            camara.setDisplayOrientation(90);
            parametros.setRotation(90);
        } else {
            parametros.set("orientation", "landscape");
            camara.setDisplayOrientation(0);
            parametros.setRotation(0);
        }

        parametros.setPictureSize(mTamano.width, mTamano.height);
        camara.setParameters(parametros);
        try{
            camara.setPreviewDisplay(holder);
            camara.startPreview();
        } catch (IOException e){
            e.printStackTrace();
        }



    }
}
