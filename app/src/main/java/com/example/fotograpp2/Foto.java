package com.example.fotograpp2;

import java.util.Date;

public class Foto {
    private int id;
    private String path;
    private String fecha;
    private String tipo;
    private String nombre;

    private long idBD;

    public Foto(String path, String fecha, String tipo){
        this.path = path;
        this.fecha = fecha;
        this.tipo = tipo;
        this.nombre = this.getTipo() + "_" + fecha.toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) { this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public long getIdBD() {
        return idBD;
    }

    public void setIdBD(long idBD) {
        this.idBD = idBD;
    }
}
