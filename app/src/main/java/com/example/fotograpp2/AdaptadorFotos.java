package com.example.fotograpp2;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class AdaptadorFotos extends RecyclerView.Adapter<AdaptadorFotos.ExampleViewHolder> {
    private List<Foto> mListaDeFotos;
    public AyudanteBD bd;

    public static class ExampleViewHolder extends RecyclerView.ViewHolder{
        public TextView textoNombre;
        public TextView textoTipo;
        public TextView textoFecha;

        public ExampleViewHolder(@NonNull View itemView) {
            super(itemView);
            textoNombre = itemView.findViewById(R.id.textoNombre);
            textoTipo = itemView.findViewById(R.id.textoTipo);
            textoFecha = itemView.findViewById(R.id.textoFecha);
        }
    }

    public AdaptadorFotos(List<Foto> listaDeFotos){
        mListaDeFotos = listaDeFotos;
    }

    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fila_foto, viewGroup,false);
        ExampleViewHolder evh = new ExampleViewHolder(v);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder exampleViewHolder, int i) {
        Foto fotoActual = mListaDeFotos.get(i);
        exampleViewHolder.textoNombre.setText(fotoActual.getNombre());
        exampleViewHolder.textoFecha.setText(fotoActual.getFecha());
        exampleViewHolder.textoTipo.setText(fotoActual.getTipo());
    }

    @Override
    public int getItemCount() {
        return mListaDeFotos.size();
    }
}
